from django.db import models
from django.core.validators import RegexValidator


class Hiburan(models.Model):

    nama = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    budget = models.CharField(max_length=15)
    kontak = models.CharField(max_length=15, default='080000000000')
    alamat = models.TextField()
    jam_buka = models.CharField(max_length=50,null = True)
    jam_tutup = models.CharField(max_length=50,null = True)
    foto = models.CharField(max_length=50, null = True)

    def __str__(self):
        return self.nama

class Fotos(models.Model):
    link_foto = models.CharField(max_length=100)
    tempat_hiburan = models.ForeignKey(Hiburan, null=True, on_delete= models.CASCADE)
    
    def __str__(self):
        return self.link_foto

