from django.shortcuts import render, get_object_or_404, render,  redirect
from .forms import CreateHiburan, CreateFoto
from .models import Hiburan, Fotos
from . import models, forms
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import JsonResponse



# Create your views here.

def index(request):
    # print(Hiburan.objects.all())
    return render(request, 'hiburan/index.html', {"hiburan" : Hiburan.objects.all()})

@login_required(login_url='/account/login/')
def addTempat(request):    
    data = {'message':''}
    form = CreateHiburan()
    if request.method == 'POST':
        form = forms.CreateHiburan(request.POST)
        if form.is_valid():
            hbrn = models.Hiburan()
            hbrn.nama = form.cleaned_data['nama']
            hbrn.status = form.cleaned_data['status']
            hbrn.budget = form.cleaned_data['budget']
            hbrn.kontak = form.cleaned_data['kontak']
            hbrn.alamat = form.cleaned_data['alamat']
            hbrn.jam_buka = form.cleaned_data['jam_buka']
            hbrn.jam_tutup = form.cleaned_data['jam_tutup']
            hbrn.foto = form.cleaned_data['foto']
            hbrn.save()        
            data['message'] = 'Tempat Berhasil Ditambahkan'
            return JsonResponse(data)
            return redirect('/hiburan')
        else:
            data['message'] = 'invalid form'
    return render(request, 'hiburan/addTempat.html', {'form':form})

@login_required(login_url='/account/login/')
def addFoto(request, id):
    if request.method == "POST":
        form = CreateFoto(request.POST)
        if form.is_valid():
            fotonya = Fotos(tempat_hiburan=Hiburan.objects.get(pk=id))
            fotonya.link_foto = form.data['link_foto']
            fotonya.save()  
        return redirect('/hiburan/detail/'+str(id))
    form = CreateFoto()
    return render(request, "hiburan/addFoto.html", {'data': form})


def detail(request, id):
    obj = get_object_or_404(Hiburan, id=id)
    fotos = Fotos.objects.filter(tempat_hiburan=obj)
    return render(request, 'hiburan/detail.html', {"obj":obj, "fotos":fotos})

@login_required(login_url='/account/login/')
def update(request, id):
    hbrn = Hiburan.objects.get(pk=id)
    form = CreateHiburan()
    if request.method == 'POST':
        data = request.POST
        hbrn.status = data['status']
        hbrn.budget = data['budget']
        hbrn.kontak = data['kontak']
        hbrn.alamat = data['alamat']
        hbrn.save()
        return redirect('/hiburan/detail/'+str(id))
    context = {
        'form' : form
    }
    return render(request, "hiburan/update.html", context)

def hiburan_api(request):
    data = {'total_items': 0, 'items': []}
    hiburans = Hiburan.objects.all()
    for hiburan in hiburans:
        nama = hiburan.nama
        status = hiburan.status
        budget = hiburan.budget
        kontak = hiburan.kontak
        alamat = hiburan.alamat
        jam_buka = hiburan.jam_buka
        jam_tutup = hiburan.jam_tutup
        foto = "https://gitlab.com/fadhil.rasendriya/gocek-v3/-/blob/master/restoran/views.py"
        id = hiburan.id
        url = f"/hiburan/detail/{id}"
        data['items'].append({'nama': nama, 'alamat': alamat, 'foto': foto, 'url': url, 'status': status, 'budget': budget, 'kontak': kontak, 'jam_buka': jam_buka, 'jam_tutup': jam_tutup})
        data['total_items'] += 1
    return JsonResponse(data)

def foto_api(request):
    data = {'total_items': 0, 'items': []}
    hiburan_id = request.GET.get('id')
    hiburan= get_object_or_404(Hiburan, id=hiburan_id)
    fotos = Fotos.objects.all()
    for foto in fotos:
        if hiburan == foto.tempat_hiburan:
            link_foto = foto.link_foto
            data['items'].append({'link_foto': link_foto})
            data['total_items'] += 1
    return JsonResponse(data)
