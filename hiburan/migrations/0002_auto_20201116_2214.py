# Generated by Django 3.1.1 on 2020-11-16 15:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hiburan', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Foto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alamatt', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='hiburan',
            name='jam_buka',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='hiburan',
            name='jam_tutup',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='hiburan',
            name='link_foto',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='hiburan',
            name='alamat',
            field=models.TextField(),
        ),
    ]
