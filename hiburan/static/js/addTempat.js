$(document).ready(function () {
    $(document).on("submit", "#addForm", function (e) {
        e.preventDefault();
        let form = document.getElementById("addForm");
        let formData = new FormData(form);
        $.ajax({
            method: 'POST',
            url: '/hiburan/addTempat/',
            enctype: 'multipart/form-data',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                console.log(result)
                let message = result['message'];
                if (message === 'Tempat Berhasil Ditambahkan') {
                    alert(message);     
                    window.location.replace('/hiburan/');
                } else {
                    let messageBox = $('#messageBox');
                    messageBox.empty();
                    messageBox.append(
                        `
                    <div class="form-group row">
                         <p style="color:red" class="form-control-label">${message}</p>
                    </div>
                    `);
                }
            }
        });
    });
});
