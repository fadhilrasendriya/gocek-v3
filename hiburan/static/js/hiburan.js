var i = 1
$("#ubah-tema").click(function(){
    if (i) {
        i = 0
        $(".base-kategori").css("background-color", "white");
        $(".footer").css("background-color", "white");
    } else {
        i = 1
        $(".base-kategori").css("background-color", "rgba(196,196,196,0.4)");
        $(".footer").css("background-color", "rgba(196,196,196,0.4)");
    }
})

$("#id_nama").keyup(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 50) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_status").keyup(function( event ) {
    var q = $(this).val().length;
    $("#status").text(q);
    if (q > 20) {
        $("#letter-count-status").css("color", "red")
    } else {
        $("#letter-count-status").css("color", "black")
    }
})

$("#id_budget").keyup(function( event ) {
    var q = $(this).val().length;
    $("#budget").text(q);
    if (q > 20) {
        $("#letter-count-budget").css("color", "red")
    } else {
        $("#letter-count-budget").css("color", "black")
    }
})

$("#id_kontak").keyup(function( event ) {
    var q = $(this).val().length;
    $("#kontak").text(q);
    if (q > 13) {
        $("#letter-count-kontak").css("color", "red")
    } else {
        $("#letter-count-kontak").css("color", "black")
    }
})

$("#id_alamat").keyup(function( event ) {
    var q = $(this).val().length;
    $("#alamat").text(q);
    if (q > 100) {
        $("#letter-count-alamat").css("color", "red")
    } else {
        $("#letter-count-alamat").css("color", "black")
    }
})

$("#id_jam_buka").keyup(function( event ) {
    var q = $(this).val().length;
    $("#jam-buka").text(q);
    if (q > 10) {
        $("#letter-count-jam-buka").css("color", "red")
    } else {
        $("#letter-count-jam-buka").css("color", "black")
    }
})

$("#id_jam_tutup").keyup(function( event ) {
    var q = $(this).val().length;
    $("#jam-tutup").text(q);
    if (q > 10) {
        $("#letter-count-jam-tutup").css("color", "red")
    } else {
        $("#letter-count-jam-tutup").css("color", "black")
    }
})

$("#id_foto").keyup(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 400) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})

$("#id_link_foto").keyup(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 400) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})

$("#id_link_foto").keydown(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 400) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})

$("#id_nama").keydown(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 50) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_status").keydown(function( event ) {
    var q = $(this).val().length;
    $("#status").text(q);
    if (q > 20) {
        $("#letter-count-status").css("color", "red")
    } else {
        $("#letter-count-status").css("color", "black")
    }
})

$("#id_budget").keydown(function( event ) {
    var q = $(this).val().length;
    $("#budget").text(q);
    if (q > 20) {
        $("#letter-count-budget").css("color", "red")
    } else {
        $("#letter-count-budget").css("color", "black")
    }
})

$("#id_kontak").keydown(function( event ) {
    var q = $(this).val().length;
    $("#kontak").text(q);
    if (q > 13) {
        $("#letter-count-kontak").css("color", "red")
    } else {
        $("#letter-count-kontak").css("color", "black")
    }
})

$("#id_alamat").keydown(function( event ) {
    var q = $(this).val().length;
    $("#alamat").text(q);
    if (q > 100) {
        $("#letter-count-alamat").css("color", "red")
    } else {
        $("#letter-count-alamat").css("color", "black")
    }
})

$("#id_jam_buka").keydown(function( event ) {
    var q = $(this).val().length;
    $("#jam-buka").text(q);
    if (q > 10) {
        $("#letter-count-jam-buka").css("color", "red")
    } else {
        $("#letter-count-jam-buka").css("color", "black")
    }
})

$("#id_jam_tutup").keydown(function( event ) {
    var q = $(this).val().length;
    $("#jam-tutup").text(q);
    if (q > 10) {
        $("#letter-count-jam-tutup").css("color", "red")
    } else {
        $("#letter-count-jam-tutup").css("color", "black")
    }
})

$("#id_foto").keydown(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 400) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})
