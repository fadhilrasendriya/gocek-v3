[![Coverage](https://gitlab.com/fadhil.rasendriya/gocek-v3/badges/master/coverage.svg)](https://gitlab.com/fadhil.rasendriya/gocek-v3/pipelines)

Nama Anggota : 
-Said Nizamudin 
-Jihan Alfiyyah Munajat 
-Rezaldy Ahmad Maulana 
-Fadhil Rasendriya Prabowo

Link Heroku : gocek.herokuapp.com

Link Wireframe & Prototype : https://www.figma.com/file/bsgxOoHylNGYSSqarJaBCo/TK1-PPW-Gocek?node-id=1%3A4

Link Persona : https://drive.google.com/file/d/1l8q9XoHiBYIMX00EEm6dGgNX_SXXMORf/view?usp=sharing

Deskripsi : Berangkat dari masalah dimana Masyarakat Jakarta kesulitan untuk mendapat informasi terkait suatu tempat yang ingin dikunjungi,
Sehingga mereka mau tidak mau harus mengunjungi tempat tersebut untuk mengecek. Kami datang dengan Gocek.
GoCek adalah aplikasi yang bertujuan untuk memberi informasi terkait tempat-tempat disekitar Jakarta. 
Mulai dari keadaan tempat tersebut selama covid-19, baik waktu buka dan waktu tutup, protokol kesehatan disana, 
gimana keadaanya, boleh masuk atau ga? Jadi, secara keseluruhan gocek ini adalah aplikasi yang dirancang untuk mendata tempat-tempat
baik wisata ataupun hiburan agar masyarakat bisa melihat keadaan tempat yang ingin mereka kunjungi tanpa harus ketempat tersebut dan 
tentu saja ter up-to-date setiap harinya.

Fitur Umum: 
- Tambah Tempat
- Cari Tempat
- Update Tempat
- Detail Tempat


Fitur Khusus:
- Data link rumah sakit dan menerima pasien Covid atau tidak
- Data penjual di pasar/tempat belanja beserta pemesanan dengan direct call
- Daftar makanan beserta gambar dan informasi lain
- Foto-foto menarik dari tempat hiburan yang bersangkutan dan budget harga

Dengan fitur-fitur tersebut, kami berharap Masyarakat dapat berpartisipasi sebagai pemberi informasi terhadap tempat
yang ingin dikunjungi dan juga mendapat informasi yang bermanfaat dan tervalidasi
