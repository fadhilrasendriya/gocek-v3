from django.urls import path
from . import views
from .views import *

app_name = 'rumah_sakit'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah-rs/', views.tambahRS, name='tambahRS'),
    path('update-rumah-sakit/<int:pk>/', views.updateRS, name='updateRS'),
    path('detail-rumah-sakit/<int:pk>/', views.detailRS, name='detailRS'),
    path('detail-rumah-sakit/<int:pk>/tambah-dokter/', views.tambahDokter, name='tambahDokter'),
    path('getrslist/', views.rs_api, name='rs_api'),
    path('doctorapi/', views.doctor_api, name='doctor_api')
]