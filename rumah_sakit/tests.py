from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import RumahSakit, Dokter
from .apps import *
from .forms import *
from . import views

import datetime

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(RumahSakitConfig.name, "rumah_sakit")

# Create your tests here.
class TestRoute(TestCase):
    # template rs obj
    def rsDummy(self):
        test = 'test'
        rs = RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test, link_rs = test, link_foto = test)
        return rs
    
    def dokter_dummy(self):
        test = 'test'
        rs = self.rsDummy()
        test_jam_buka = datetime.time(0,0,0)
        test_jam_tutup = datetime.time(1,0,0)
        dokter = Dokter.objects.create(rs = rs, nama_dokter = test, spesialis = test, layanan_start = test_jam_buka, layanan_end = test_jam_tutup, phone_number = 1)
        return dokter
        

    def test_function_used_main(self):
        response = resolve('/rumah-sakit/')
        self.assertEqual(response.func, views.index)

    def test_function_tambah_rs(self):
        response = resolve('/rumah-sakit/tambah-rs/')
        self.assertEqual(response.func, views.tambahRS)    

    def test_function_used_update_rs(self):
        rs = self.rsDummy()
        response = resolve('/rumah-sakit/update-rumah-sakit/' + str(rs.pk) + '/')
        self.assertEqual(response.func, views.updateRS)

    def test_function_used_detail_rs(self):
        rs = self.rsDummy()
        response = resolve('/rumah-sakit/detail-rumah-sakit/' + str(rs.pk) + '/')
        self.assertEqual(response.func, views.detailRS)

    def test_function_used_tambah_dokter_rs(self):
        rs = self.rsDummy()
        response = resolve('/rumah-sakit/detail-rumah-sakit/' + str(rs.pk) + '/tambah-dokter/')
        self.assertEqual(response.func, views.tambahDokter)
    
    # template register account
    def registerDummy(self):
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@tkmail.com'}
        self.client.post('/account/signup/', data=data)
    
    def test_template_used_main(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/rumah-sakit/')
        self.assertTemplateUsed(response, 'index.html')   
    
    def test_template_used_tambah_rs(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/rumah-sakit/tambah-rs/')
        self.assertTemplateUsed(response, 'tambahRS.html')    
    
    def test_template_used_update_rs(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/update-rumah-sakit/'+str(rs.pk)+"/", follow=True)
        self.assertTemplateUsed(response, 'updateRS.html')
    
    
    def test_template_used_detail_rs(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/", follow=True)
        self.assertTemplateUsed(response, 'detailRS.html')

    def test_template_used_tambah_dokter(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/tambah-dokter/", follow=True)
        self.assertTemplateUsed(response, 'tambahDokter.html')


  #  def test_landing_rs_before_login(self):
  #      response = self.client.get('/rumah-sakit/', follow=True)
  #      html_response = response.content.decode('utf8')
  #      self.assertIn('username', html_response)
   #     self.assertIn('login', html_response)

    def test_tambah_rs_before_login(self):
        response = self.client.get('/rumah-sakit/tambah-rs/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_update_rs_before_login(self):
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/update-rumah-sakit/'+str(rs.pk)+"/", follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)
        pass

#    def test_detail_rs_before_login(self):
 #       rs = self.rsDummy()
  #      response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/", follow=True)
  #      html_response = response.content.decode('utf8')
  #      self.assertIn('username', html_response)
  #      self.assertIn('login', html_response)


    def test_tambah_dokter_before_login(self):
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/tambah-dokter/", follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_landing_rs_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/rumah-sakit/', follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_tambah_rs_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/rumah-sakit/tambah-rs/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_update_rs_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/update-rumah-sakit/'+str(rs.pk)+"/", follow=True)
        self.assertEqual(response.status_code, 200)

    def test_detail_rs_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/", follow=True)
        self.assertEqual(response.status_code, 200)

    def test_tambah_dokter_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        rs = self.rsDummy()
        response = self.client.get('/rumah-sakit/detail-rumah-sakit/'+str(rs.pk)+"/tambah-dokter/", follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_get_rs_api(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.rsDummy()
        response = self.client.get('/rumah-sakit/getrslist/')
        json_response = eval(response.content.decode('utf8'))
        self.assertEqual(json_response['items'][0].get('nama_rs'), 'test')
        '''
    def test_get_doctor_api(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dokter_dummy()
        data = {'id': '1'}
        response = self.client.get('/rumah-sakit/doctorapi/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertEqual(json_response['items'][0].get('nama_dokter'), 'test')
'''

class TestActivity(TestCase):
    def test_model_rumah_sakit_can_create(self):
        test = 'test'
        RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        counter = RumahSakit.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_model_dokter_can_create(self):
        test = 'test'
        test_jam_buka = datetime.time(0,0,0)
        test_jam_tutup = datetime.time(1,0,0)
        rs = RumahSakit.objects.create(nama_rs=test, status = test, covid = test, test_rs = test, alamat = test)
        Dokter.objects.create(rs = rs, nama_dokter = test, spesialis = test, layanan_start = test_jam_buka, layanan_end = test_jam_tutup, phone_number = 1)
        counter = Dokter.objects.all().count()
        self.assertEqual(counter, 1)



    
