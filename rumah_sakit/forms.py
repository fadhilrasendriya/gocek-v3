from django import forms
from .models import *

class RumahSakitForm(forms.ModelForm):
    nama_rs = forms.CharField(label = 'Nama Rumah Sakit', max_length=100, required = True, 
        widget = forms.TextInput(attrs={
            'placeholder' : 'Rumah Sakit Sentosa',
            'class' : 'form-control',
            'required': True,
        })
    )

    link_foto = forms.URLField(label = 'Foto', required = True,
        widget = forms.TextInput(attrs={
            'placeholder' : 'masukkan link foto rumah sakit',
            'class' : 'form-control',
            'required': True,
        })
    )

    link_rs = forms.URLField(label = 'Link Rumah Sakit', required = True,
        widget = forms.TextInput(attrs={
            'placeholder' : 'masukkan link website rumah sakit',
            'class' : 'form-control',
            'type' : 'text',
        })
    )

    alamat = forms.CharField(label = 'Alamat Rumah Sakit',
        widget = forms.TextInput(attrs={
            'placeholder' : 'Jl. Pramuka, Jakarta Timur',
            'class' : 'form-control',
            'type' : 'text',
            'required': True,
        })
    )

    class Meta:
        model = RumahSakit
        fields = [
            'nama_rs',
            'status',
            'link_foto',
            'link_rs',
            'covid',
            'test_rs',
            'alamat',
        ]

class DokterForm(forms.ModelForm):  
    nama_dokter = forms.CharField(label = 'Nama Dokter', max_length=50, required = True, 
        widget = forms.TextInput(attrs={
            'placeholder' : 'Dr Adi Wijaya',
            'class' : 'form-control',
            'type' : 'text',
            'required': True,
        })
    )
    spesialis = forms.CharField(label = 'Spesialis', max_length=50, required = True, 
        widget = forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Sp.OG/SP.A/SP.KK/Psikiater/dll',
            'required' : True,
        })
    )
    layanan_start = forms.TimeField(label = 'Jam Mulai Layanan', widget = forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : '08:00',
            'required' : True,
        }))
    layanan_end = forms.TimeField(label = 'Jam Berakhir Layanan',widget = forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : '12:00',
            'required' : True,
        }))
    phone_number = forms.CharField(label = 'No. Whatsapp ',
        widget = forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : '62812xxxxxxxx'
        })
    )

    class Meta:
        model = Dokter
        fields = [
            'nama_dokter',
            'spesialis',
            'layanan_start',
            'layanan_end',
            'phone_number'
        ]
