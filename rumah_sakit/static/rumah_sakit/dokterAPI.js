$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {
            'id': $('#RSid').val()
        },
        url: '/rumah-sakit/doctorapi/',
        success: function (result) {
            let dokter = $('#dokterRow');
            dokterRow.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let nama = item['nama_dokter'];
                let spesialis = item['kontakPenjual'];
                let jam_start = item['layanan_start'];
                let jam_end = item['layanan_end'];
                let nomor = item['phone_number'];
                dokter.append(
                    `
                    <tr>
                        <td>${nama}</td>
                        <td>${spesialis}</td>
                        <td>${jam_start} - ${jam_end}</td>
                        <td><a href="https://wa.me/${nomor}" target="_blank" class="btn btn-success" style="border-radius: 20px;">Whatsapp</a></td>
                    </tr>
                    `
                );
            }
        }
    })
});
