from django.shortcuts import render, redirect, get_object_or_404
from . import forms, models
from django.contrib import messages
from .models import RumahSakit, Dokter
from .forms import RumahSakitForm, DokterForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

# Create your views here.

def index(request):
    data_rs = RumahSakit.objects.all()
    response = {'data_rs' : data_rs}
    return render(request, 'index.html', response)

@login_required(login_url='/account/login/')
def tambahRS(request):
    if request.method == "POST" or None:
        formRS = forms.RumahSakitForm(request.POST)
        if formRS.is_valid():
            dataRS = models.RumahSakit()
            dataRS.nama_rs = formRS.cleaned_data['nama_rs']
            dataRS.status = formRS.cleaned_data['status']
            dataRS.link_foto = formRS.cleaned_data['link_foto']
            dataRS.link_rs = formRS.cleaned_data['link_rs']
            dataRS.covid = formRS.cleaned_data['covid']
            dataRS.test_rs = formRS.cleaned_data['test_rs']
            dataRS.alamat = formRS.cleaned_data['alamat']
            dataRS.save()
            data['message'] = 'Rumah Sakit berhasil ditambahkan'
            return JsonResponse(data)
            return redirect('/rumah-sakit/')
        else:
            data['message'] = 'Invalid Form'

    response = {'form_rs' : forms.RumahSakitForm}
    return render(request, 'tambahRS.html', response)
        
@login_required(login_url='/account/login/')
def tambahDokter(request, pk):
    if request.method == "POST" or None:
        formDokter = forms.DokterForm(request.POST)
        if formDokter.is_valid():
            dataDokter = models.Dokter()
            dataDokter.rs = models.RumahSakit.objects.get(pk=pk)
            dataDokter.nama_dokter = formDokter.cleaned_data['nama_dokter']
            dataDokter.spesialis = formDokter.cleaned_data['spesialis']
            dataDokter.layanan_start = formDokter.cleaned_data['layanan_start']
            dataDokter.layanan_end = formDokter.cleaned_data['layanan_end']
            dataDokter.phone_number = formDokter.cleaned_data['phone_number']
            dataDokter.save()
            return redirect('/rumah-sakit/detail-rumah-sakit/'+str(pk))
        else:
            return render(request, "tambahDokter.html", {'form_dokter': formDokter, 'errors': formDokter.errors})
    else: 
        args = {'form_dokter' : forms.DokterForm}
        return render(request, 'tambahDokter.html', args)

def detailRS(request, pk):
    rs = RumahSakit.objects.get(pk=pk)
    dokter = Dokter.objects.filter(rs=rs)
    response = {'rs':rs, 'dokter':dokter}
    return render(request, 'detailRS.html', response)
    
@login_required(login_url='/account/login/')
def updateRS(request, pk):
    rs = RumahSakit.objects.get(pk=pk)
    formRS = RumahSakitForm()
    if request.method == "POST" or None:
        formRS = forms.RumahSakitForm(request.POST)
        if not formRS.is_valid():
            rs.status = formRS.cleaned_data['status']
            rs.covid = formRS.cleaned_data['covid']
            rs.test_rs = formRS.cleaned_data['test_rs']
            rs.alamat = formRS.cleaned_data['alamat']
            rs.save()
            return redirect('/rumah-sakit/detail-rumah-sakit/'+str(pk))
    response = {'form_rs' : formRS, 'rs': rs}
    return render(request, 'updateRS.html', response)

def rs_api(request):
    data = {'total_items':0, 'items':[]}
    rs_all = RumahSakit.objects.all()
    for rs in rs_all:
        nama_rs = rs.nama_rs
        status = rs.status
        link_foto = rs.link_foto
        link_rs = rs.link_rs
        covid = rs.covid
        test_rs = rs.test_rs
        alamat = rs.alamat
        pk = rs.pk
        url = f'rumah-sakit/detail/{pk}'
        data['items'].append({'nama_rs':nama_rs, 'status':status, 'link_foto':link_foto, 'link_rs':link_rs, 'covid':covid, 'test_rs':test_rs, 'alamat':alamat, 'url':url})
        data['total_items'] += 1
    return JsonResponse(data)

def doctor_api(request):
    data = {'total_items':0, 'items':[]}
    rs_pk = request.GET.get('pk')
    rs = get_object_or_404(RumahSakit, pk = rs_pk)
    doctors = Dokter.objects.all()
    for doctor in doctors:
        if rs == doctor.rs:
            nama_dokter = doctor.nama_dokter
            spesialis = doctor.spesialis
            layanan_start = doctor.layanan_start
            layanan_end = doctor.layanan_end
            phone_number = doctor.phone_number
            data['items'].append({'nama_dokter':nama_dokter, 'spesialis':spesialis, 'layanan_start':layanan_start, 'layanan_end':layanan_end, 'phone_number':phone_number})
            data['total_items'] += 1
    return JsonResponse(data)

