from django.urls import path
from . import views


app_name = 'hello_world'

urlpatterns = [
    path('', views.index, name='index'),
    path('cektempat/', views.cek_tempat, name='cek_tempat'),
    path('bagiinformasi/', views.bagi_informasi, name='bagi_informasi'),
    
]
