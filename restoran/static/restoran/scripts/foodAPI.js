$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {
            'id': $('#restoranId').val()
        },
        url: '/restoran/foodapi/',
        success: function (result) {
            let foodBox = $('#foodRow');
            foodBox.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let nama = item['nama'];
                let picture = item['picture'];
                let harga = item['harga'];
                foodBox.append(
                    `
                        <div class="col card text-dark align-items-center">
                            <div class="card-body cardSelector">
                                <!--foto restoran -->
                                <img class="card-img-top" src="${picture}"
                                     alt="Card image">
                                <div class="card-title"><strong>${nama}</strong></div>
                                <br>
                                <div class="card-title">${harga}</div>
                            </div>
                        </div>
                    `
                );
            }
        },
        complete: function () {
            $('.cardSelector').hover(
                function () {
                    $(this).parent().addClass("hoverColor");
                }, function () {
                    $(this).parent().removeClass("hoverColor");
                });
        }
    });
});