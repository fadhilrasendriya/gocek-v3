from django.db import models


class Restoran(models.Model):

    nama = models.CharField(max_length=100)
    alamat = models.TextField()
    picture = models.ImageField(upload_to='images/')
    jenis_pesanan = models.CharField(max_length=100, default='Dine in & Take-away')
    kategori = models.CharField(max_length=50, default='Fast food')
    telepon = models.CharField(max_length=15, default='000000000000')
    time_open = models.TimeField()
    time_close = models.TimeField()


class Makanan(models.Model):
    id = models.AutoField(primary_key=True)

    nama = models.CharField(max_length=100)
    harga = models.IntegerField()
    picture = models.ImageField(upload_to='images/')
    restoran = models.ForeignKey(Restoran, on_delete=models.CASCADE)
