from . import views
from django.urls import path
app_name = 'sistemlogin'

urlpatterns = [
    path('login/', views.log_in, name='log_in'),
    path('signup/', views.sign_up, name='sign_up'),
    path('logout/', views.log_out, name='log_out'),
]
