from django.shortcuts import render, get_object_or_404, render,  redirect
from .models import Penjual, Pasar
from .forms import PasarForms, PenjualForms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

def pasar_mainpage(request):
    queryset = Pasar.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "pasar_mainpage.html", context)
@login_required(login_url='/account/login/')
def pasar_tambah(request):
    data = {'message':''}
    form = PasarForms(request.POST or None)
    if form.is_valid(): #validator
        form.save()
        data['message'] = 'Pasar berhasil ditambahkan'
        return JsonResponse(data)
        return redirect('/pasar/')
    else:
        data['message'] = 'invalid form'
    context = {
        'form' : form
    }
    return render(request, "pasar_form.html", context)

@login_required(login_url='/account/login/')
def penjual_tambah(request, id=1):
    if request.method == "POST":
        form = PenjualForms(request.POST or None)
        if form.is_valid():
            penjualValid = Penjual(pasarPenjual=Pasar.objects.get(pk=id), 
            namaPenjual=form.data['namaPenjual'],
            kontakPenjual = form.data['kontakPenjual'],
            jenisJualan = form.data['jenisJualan'])
            penjualValid.save()  
        else:
            messages.error(request, 'invalid form')
        return redirect('/pasar/detail/'+str(id))
    form = PenjualForms()
    return render(request, "penjual_form.html", {'penjual': form})

def pasar_detail(request, id):
    obj = get_object_or_404(Pasar, id=id)
    kumpulanPenjual = Penjual.objects.filter(pasarPenjual=obj)
    return render(request, 'pasar_detail.html', {"penjual":kumpulanPenjual, "obj":obj})

@login_required(login_url='/account/login/')
def pasar_update(request, id):
    pasar = Pasar.objects.get(pk=id)
    form = PasarForms(instance=pasar)
    if request.method == 'POST':
        form = PasarForms(request.POST, instance=pasar)
        if form.is_valid():
            form.save()
            return redirect('/pasar/detail/'+str(id))
    context = {
        'form' : form
    }
    return render(request, "pasar_update_form.html", context)


def pasar_api(request):
     data = {'total_items': 0, 'items': []}
     semuaPasar = Pasar.objects.all()
     for pasar in semuaPasar:
        nama = pasar.namaPasar
        kontak = pasar.hotlinePasar
        foto = pasar.fotoPasar
        buka = pasar.jambukaPasar.strftime("%H:%M")
        tutup = pasar.jamtutupPasar.strftime("%H:%M")
        status = pasar.statuscovidPasar
        alamat = pasar.alamatPasar
        id_pasar = pasar.id
        url = f"/pasar/detail/{id_pasar}"
        data['items'].append({'namaPasar': nama, 'hotlinePasar' : kontak, 'fotoPasar': foto, 
        'jambukaPasar': buka, 'jamtutupPasar': tutup, 'statuscovidPasar':status,'alamatPasar':alamat, 'url': url})
        data['total_items'] += 1
     return JsonResponse(data)

def penjual_api(request):
    data = {'total_items': 0, 'items': []}
    pasar_id = request.GET.get('id')
    pasar = get_object_or_404(Pasar, id=pasar_id)
    semuaPenjual = Penjual.objects.all()
    for penjual in semuaPenjual:
        if pasar == penjual.pasarPenjual:
            nama = penjual.namaPenjual
            kontak = penjual.kontakPenjual
            jenis = penjual.jenisJualan
            data['items'].append({'namaPenjual': nama, 
            'kontakPenjual': kontak, 'jenisJualan': jenis})
            data['total_items'] += 1
    return JsonResponse(data)
