var i = 1
$("#ubah-tema").click(function(){
    if (i) {
        i = 0
        $(".base-kategori").css("background-color", "#404040");
        $("body").css("background-color", "#282828");
        $(".tema").css("background-color", "#282828");
        $(".footer").css("background-color", "282828");
    } else {
        i = 1
        $(".base-kategori").css("background-color", "rgba(196,196,196,0.4)");
        $("body").css("background-color", "white");
        $(".tema").css("background-color", "white");
        $(".footer").css("background-color", "rgba(196,196,196,0.4)");
        
    }
})

$("#id_namaPasar").keyup(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 25) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_hotlinePasar").keyup(function( event ) {
    var q = $(this).val().length;
    $("#telpon").text(q);
    if (q > 13) {
        $("#letter-count-hotl").css("color", "red")
    } else {
        $("#letter-count-hotl").css("color", "black")
    }
})

$("#id_jambukaPasar").keyup(function( event ) {
    var q = $(this).val().length;
    $("#buka").text(q);
    if (q > 5) {
        $("#letter-count-buka").css("color", "red")
    } else {
        $("#letter-count-buka").css("color", "black")
    }
})

$("#id_jamtutupPasar").keyup(function( event ) {
    var q = $(this).val().length;
    $("#tutup").text(q);
    if (q > 5) {
        $("#letter-count-tutup").css("color", "red")
    } else {
        $("#letter-count-tutup").css("color", "black")
    }
})

$("#id_alamatPasar").keyup(function( event ) {
    var q = $(this).val().length;
    $("#alamats").text(q);
    if (q > 100) {
        $("#letter-count-alamat").css("color", "red")
    } else {
        $("#letter-count-alamat").css("color", "black")
    }
})

$("#id_namaPenjual").keyup(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 50) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_kontakPenjual").keyup(function( event ) {
    var q = $(this).val().length;
    $("#telpon").text(q);
    if (q > 13) {
        $("#letter-count-hotl").css("color", "red")
    } else {
        $("#letter-count-hotl").css("color", "black")
    }
})

$("#id_jenisJualan").keyup(function( event ) {
    var q = $(this).val().length;
    $("#jual").text(q);
    if (q > 50) {
        $("#letter-count-jual").css("color", "red")
    } else {
        $("#letter-count-jual").css("color", "black")
    }
})

$(document).ready(function(){
    $(".infodetail").hover(function(){
      $(this).css("background-color", "#4180E2");
      }, function(){
      $(this).css("background-color", "#F2F2F2");
    });
  });

  
  $(document).ready(function(){
    $(".ubah-card").hover(function(){
      $(this).css("background-color", "#F2F2F2");
      }, function(){
      $(this).css("background-color", "#6E96D3");
    });
  });

  