$(document).ready(function () {
    $(document).on("submit", "#addForm", function (e) {
        e.preventDefault();
        let form = document.getElementById("addForm");
        let formData = new FormData(form);
        $.ajax({
            method: 'POST',
            url: '/pasar/tambah/',
            enctype: 'multipart/form-data',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                let message = result['message'];
                if (message === 'Pasar berhasil ditambahkan') {
                    alert(message);
                    window.location.replace('/pasar/');
                } else {
                    let messageBox = $('#messageBox');
                    messageBox.empty();
                    messageBox.append(
                        `
                    <div class="form-group row">
                         <p style="color:red" class="form-control-label">${message}</p>
                    </div>
                    `);
                }
            }
        });
    });
});
