$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {
            'id': $('#pasarId').val()
        },
        url: '/pasar/penjualapi/',
        success: function (result) {
            let penjualBox = $('#penjualRow');
            penjualBox.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let nama = item['namaPenjual'];
                let kontak = item['kontakPenjual'];
                let jenis = item['jenisJualan'];
                penjualBox.append(
                    `
                    <tr>
                    <th scope="row" style="color: grey;">${nama}</th>
                    <td style="color: grey;">${jenis}</td>
                    <td><a id="detail" href="https://api.whatsapp.com/send?phone=62${kontak}"
                            class="btn btn-success" style="border-radius: 18px;">Pesan via Whatsapp</a></td>
                    </tr>
                    `
                );
            }
        }
    })
});
