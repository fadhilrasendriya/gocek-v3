from django.db import models

# Create your models here.

class Pasar(models.Model):
    namaPasar = models.CharField(max_length=25)
    hotlinePasar = models.CharField(max_length=13)
    fotoPasar = models.CharField(max_length=200, null=True)
    jambukaPasar = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    jamtutupPasar = models.TimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    CHOICES = (('Zona Hijau','Zona Hijau'),
            ('Zona Kuning','Zona Kuning'),
            ('Zona Oranye','Zona Oranye'),
            ('Zona Merah','Zona Merah'),
            ('Tidak Diketahui','Tidak Diketahui'),)
    statuscovidPasar = models.CharField(max_length = 20, choices = CHOICES)
    alamatPasar = models.CharField(blank=True, null=True, max_length=100)

    def __str__(self):
        return self.namaPasar


class Penjual(models.Model):
    namaPenjual = models.CharField(max_length=50)
    kontakPenjual = models.CharField(max_length=13)
    jenisJualan = models.CharField(max_length=50)
    pasarPenjual = models.ForeignKey(Pasar, null=True, on_delete= models.CASCADE)
    
    def __str__(self):
        return self.namaPenjual